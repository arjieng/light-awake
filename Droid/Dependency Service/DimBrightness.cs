﻿using System;
using System.Threading.Tasks;
using System.Threading;
using lightawake.Droid;
using Android.App;
using Android.Content;
using Xamarin.Forms;
using Android.Provider;
using Android.Views;
using Android.OS;

[assembly: Dependency(typeof(DimBrightness))]

namespace lightawake.Droid
{
    
    public class DimBrightness : iDimScreen
    {
        static bool lights;
        
        public void DimScreen(bool lightsOff)
        {
			         lights = lightsOff;
			int currentIntensity = Settings.System.GetInt(((Activity)Forms.Context).ContentResolver, Settings.System.ScreenBrightness);
			        if (lights)
			        {
			ContentResolver cResolver = ((Activity)Forms.Context).ContentResolver;
			Settings.System.PutInt(cResolver, Settings.System.ScreenBrightness, 0);
			        }
			        else
			        {
			ContentResolver cResolver = ((Activity)Forms.Context).ContentResolver;
			Settings.System.PutInt(cResolver, Settings.System.ScreenBrightness, 255);
			}
        }
    }

}

