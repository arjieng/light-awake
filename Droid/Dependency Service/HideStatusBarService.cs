﻿using System;
using System.Threading.Tasks;
using System.Threading;
using lightawake.Droid;
using Android.App;
using Android.Content;
using Xamarin.Forms;
using Android.Provider;
using Android.Views;
using Android.OS;

[assembly: Dependency(typeof(HideStatusBarService))]
namespace lightawake.Droid
{
    public class HideStatusBarService : iHideStatusBar
    {
        static bool hidden;
        public HideStatusBarService()
        {
        }

        public void HideStatusBar(bool isHidden)
        {
            hidden = isHidden;
            var window = ((Activity)Forms.Context).Window;
            if(hidden)
            {
                
                window.AddFlags(WindowManagerFlags.Fullscreen | WindowManagerFlags.TurnScreenOn);
               
            }
            else
            {
                window.ClearFlags(WindowManagerFlags.Fullscreen | WindowManagerFlags.TurnScreenOn);
            }
            
        }
    }
}
