﻿using System;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V4.Content;
using lightawake.Droid;
using Xamarin.Forms;
using Android.Provider;
using static Android.OS.PowerManager;

[assembly: Dependency(typeof(DisableSleepService))]
namespace lightawake.Droid
{
    public class DisableSleepService : iDisableSleep
    {
        public void DisableSleep()
        {
			//PowerManager pm = (PowerManager)Xamarin.Forms.Forms.Context.GetSystemService(Context.PowerService);
			
			//WakeLock wl = pm.NewWakeLock(WakeLockFlags.Full | WakeLockFlags.AcquireCausesWakeup | WakeLockFlags.OnAfterRelease, "lock");
   //         wl.Acquire();
			//WakeLock wl_cpu = pm.NewWakeLock(WakeLockFlags.Partial, "cpulock");
			//wl_cpu.Acquire();


			var window = ((Activity)Forms.Context).Window;
            window.SetFlags(Android.Views.WindowManagerFlags.KeepScreenOn, Android.Views.WindowManagerFlags.KeepScreenOn);
        }
    }
}
