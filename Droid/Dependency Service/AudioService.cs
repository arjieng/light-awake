﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android.Media;
using lightawake.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(AudioService))]
namespace lightawake.Droid
{
    public class AudioService : iAudio
    {
        static MediaPlayer _player;
        static bool _isPlaying;
        public AudioService()
        {
        }

        public void PlayAudioFile(bool isPlaying)
        {
            if (_player == null)
            {
                _player = MediaPlayer.Create(global::Android.App.Application.Context, Resource.Drawable.nature_ringtone);
            }

            var uri = Android.Net.Uri.Parse("android.resource://com.lightawake/drawable/nature_ringtone");

            _isPlaying = isPlaying;

            if (isPlaying)
            {
                Task.Run(() => {
                    //Thread.Sleep(30000);
                    while (_isPlaying)
                    {
                        if (!_player.IsPlaying)
                        {
                            _player.Start();
                        }
                    }
                });
            }

            else
            {
                _player.Stop();
                _player.Reset();
                _player.SetDataSource(global::Android.App.Application.Context, uri);
                _player.Prepare();
            }
        }
    }
}
