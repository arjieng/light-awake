﻿using System;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V4.Content;
using lightawake.Droid;
using Xamarin.Forms;
using static Android.OS.PowerManager;

[assembly: Dependency(typeof(AndroidNotification))]
namespace lightawake.Droid
{
    public class AndroidNotification : iNativeNotification
	{
        bool permission;
		Notification.Builder notifbuilder = new Notification.Builder(Xamarin.Forms.Forms.Context);

        public void NativeNotif(int notifId)
		{
            permission = ContextCompat.CheckSelfPermission(Forms.Context, Manifest.Permission.WakeLock) == Permission.Granted;

			PowerManager pm = (PowerManager)Xamarin.Forms.Forms.Context.GetSystemService(Context.PowerService);
			bool isScreenOn = pm.IsInteractive;
			if (isScreenOn == false)
			{
				var intent = new Intent(Forms.Context, typeof(MainActivity));

				const int pendingIntentId = 0;
				PendingIntent pendingIntent = PendingIntent.GetActivity(Xamarin.Forms.Forms.Context, pendingIntentId, intent, PendingIntentFlags.UpdateCurrent);

				WakeLock wl = pm.NewWakeLock(WakeLockFlags.Full | WakeLockFlags.AcquireCausesWakeup | WakeLockFlags.OnAfterRelease, "lock");
				wl.Acquire(60000);
				WakeLock wl_cpu = pm.NewWakeLock(WakeLockFlags.Partial, "cpulock");
				wl_cpu.Acquire(60000);

				//notifbuilder.SetSmallIcon(Resource.Drawable.icon);
				
                notifbuilder.SetContentTitle("Light Awake Alarm");
				notifbuilder.SetContentText("Next time leave Light Awake open! Use the dim button to sleep");
				notifbuilder.SetDefaults(NotificationDefaults.Sound | NotificationDefaults.Vibrate);
				notifbuilder.SetContentIntent(pendingIntent);
				Notification notification = notifbuilder.Build();
				NotificationManager notificationManager = Xamarin.Forms.Forms.Context.GetSystemService(Context.NotificationService) as NotificationManager;

				int notificationId = notifId;
				notificationManager.Notify(notificationId, notification);

                wl_cpu.Release();
			}

		}
		public void CancelNotification()
		{
			
		}

		public void NativeNotif(DateTime dateTime)
        {
            
        }

        public bool RequestPermission()
        {
            return true;
        }
    }
}
