﻿using System;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Provider;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using lightawake.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(WriteSettingsService))]
namespace lightawake.Droid
{
    public class WriteSettingsService : iWriteSettingsService
    {
        bool isOn;
        public bool isWriteSettingsOn()
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.M)
            {
                if (Settings.System.CanWrite(Xamarin.Forms.Forms.Context))
                {
                    isOn = true;
                }
                else
                {
                    isOn = false;
                }

            }
            else
            {
                isOn = ContextCompat.CheckSelfPermission(Forms.Context, Manifest.Permission.WriteSettings) == Permission.Granted;
            }
            return isOn;
		}

        public void OpenSettingsUrl()
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.M)
            {
                Intent writeSetting = new Intent(Settings.ActionManageWriteSettings, Android.Net.Uri.Parse("package:" + Forms.Context.PackageName));
                Forms.Context.StartActivity(writeSetting);
            }
            else
            {
                ActivityCompat.RequestPermissions((Activity)Forms.Context, new String[] { Manifest.Permission.WriteSettings }, MainActivity.MY_PERMISSIONS_REQUEST_WRITE_SETTINGS);
            }
        }


    }
}
