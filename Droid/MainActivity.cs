﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Threading.Tasks;
using System.Threading;
using Xamarin.Forms;
using Android.Util;
using HockeyApp.Android;

namespace lightawake.Droid
{
    [Activity(Label = "Light Awake", Icon = "@drawable/icon", Theme = "@style/MyTheme",  ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, LaunchMode = LaunchMode.SingleTop)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public const int MY_PERMISSIONS_REQUEST_WRITE_SETTINGS = 101;
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

			var density = Resources.DisplayMetrics.Density;
			App.screenWidth = Resources.DisplayMetrics.WidthPixels / density;
			App.screenHeight = (Resources.DisplayMetrics.HeightPixels / density) - 26;
			App.DeviceType = 1;


			base.OnCreate(bundle);
            global::Xamarin.Forms.Forms.Init(this, bundle);
			var window = ((Activity)Forms.Context).Window;
			window.SetFlags(Android.Views.WindowManagerFlags.KeepScreenOn, Android.Views.WindowManagerFlags.KeepScreenOn);
            LoadApplication(new App());
        }
        protected override void OnResume()
        {
            base.OnResume();
            CrashManager.Register(this, "570b4ca3cdbc4998bcee09aa477c1572");

		}

    }


}
