﻿using System;

using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using lightawake;
using lightawake.Droid;

[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace lightawake.Droid
{
    public class CustomButtonRenderer : ButtonRenderer
    {
		protected override void OnDraw(Android.Graphics.Canvas canvas)
		{
			base.OnDraw(canvas);

		}

		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);
			Control.SetAllCaps(false);
            Control.SetPadding(0,0,0,(int)5.ScaleHeight());
            Control.Gravity = Android.Views.GravityFlags.Center;
		}
    }
}
