﻿using System;

using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using lightawake;
using lightawake.Droid;

[assembly: ExportRenderer(typeof(CustomLabel), typeof(CustomLabelRenderer))]
namespace lightawake.Droid
{
    public class CustomLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            Control.SetPadding(0, (int)-40.ScaleHeight(), 0, (int)40.ScaleHeight());
			
        }
    }
}
