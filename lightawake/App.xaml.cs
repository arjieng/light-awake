using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace lightawake
{
    public partial class App : Application
    {

		public static float screenWidth { get; set; }
		public static float screenHeight { get; set; }
		public static float appScale { get; set; }
		public static int DeviceType { get; set; }
      

		public static double screenScale
		{
			get { return (screenWidth + screenHeight) / (320.0f + 568.0f); }
		}

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new HomePage());

        }

        protected override void OnStart()
        {
            // Handle when your app starts
            DependencyService.Get<iDisableSleep>().DisableSleep();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes

        }


      
      

    }
}
