﻿using System;
using Xamarin.Forms;

namespace lightawake
{
    public class Constants
    {
        //Color
        public static readonly Color MAIN_FONTCOLOR = Color.FromHex("#fdeabb");
		public static readonly Color MAIN_BACKGROUNDCOLOR = Color.FromHex("#555555");

        //Font
        public static readonly string TIME_FONT = Device.RuntimePlatform == Device.iOS ? "SFCompact" : "SFUiBold.ttf#SFUiBold";

		//Navigation Height
        public static readonly double NAV_HEIGHT = Device.RuntimePlatform == Device.iOS ? 64.ScaleHeight() : 57.ScaleHeight();

		//Border Radius
		public static readonly int BORDER_RADIUS = Device.RuntimePlatform == Device.iOS ? 20 : 30;

        //Back Button Margin
        public static readonly Thickness BACKBUTTON_MARGIN = Device.RuntimePlatform == Device.iOS ? new Thickness(10.scale(),30.scale(),0,0) : new Thickness(10.scale(), 10.scale(), 0, 0);

        //Width
        public int PICKER_WIDTH = (int)App.screenWidth;

	}
}
