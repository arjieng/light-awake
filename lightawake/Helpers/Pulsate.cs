﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace lightawake
{
    public class Pulsate
    {
        static bool pulsate;
        double timeLimit;
        public Pulsate()
        {
        }
		public async Task PulseOn( StackLayout firstStack, bool isOn)
		{
			pulsate = isOn;
   //         timeLimit = 60;
			//while (timeLimit > 0 && pulsate)
            while(pulsate)
			{
				firstStack.BackgroundColor = Color.FromHex("#3866ff");
				await Task.Delay(333);
				firstStack.BackgroundColor = Color.FromHex("#012baa");
				await Task.Delay(333);
				firstStack.BackgroundColor = Color.FromHex("#60fdff");
				await Task.Delay(333);
				timeLimit = timeLimit - 1;
			}
		
			firstStack.BackgroundColor = Color.Transparent;
           
         
		}

    }
}
