﻿using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace lightawake
{
    public class TimeModel : INotifyPropertyChanged
    {
		DateTime dateTime;

		public event PropertyChangedEventHandler PropertyChanged;

		public TimeModel()
		{
			this.DateTimes = DateTime.Now;

			Device.StartTimer(TimeSpan.FromSeconds(1), () =>
			{
				this.DateTimes = DateTime.Now;
				return true;
			});

		}

		public DateTime DateTimes
		{
			set
			{
				if (dateTime != value)
				{
					dateTime = value;

					if (PropertyChanged != null)
					{
						PropertyChanged(this, new PropertyChangedEventArgs("DateTime"));
					}
				}
			}
			get
			{
				return dateTime;
			}
		}



    }
}
