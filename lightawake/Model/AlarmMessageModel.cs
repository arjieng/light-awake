﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace lightawake
{
    public class AlarmMessageModel : INotifyPropertyChanged
    {
       
        private TimeSpan _timerAlarm;
        private bool _isAlarm;
        private bool _isCurrentlySet;
        private string _switchMessage;
        private DateTime _dateTime;

        public string messageTime
        {
            get { return timeAlarm.ToString(); }
            set
            {
                if (messageTime != value)
                {
                    messageTime = value;
                    OnPropertyChanged();
                }
            }
        }

        public TimeSpan timeAlarm
        {
            get { return _timerAlarm; }
            set 
            {
                _timerAlarm = value;
				OnPropertyChanged();
            }
        }

      
		public bool isAlarm
		{
			get { return _isAlarm; }
			set
			{
				_isAlarm = value;
				OnPropertyChanged();
                OnPropertyChanged("switchMessage");
			}
		}

		public bool isCurrentlySet
		{
			get { return _isCurrentlySet; }
			set
			{
				_isCurrentlySet = value;
				OnPropertyChanged();
			}
		}


		public string switchMessage
        {
            get { return $"Alarm {(isAlarm ? "ON" : "OFF")}"; }
           
        }

		public DateTime _DateTime
		{
			get { return _dateTime; }
			set
			{
				_dateTime = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName]string name = "")
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged(this, new PropertyChangedEventArgs(name));
		}
    }
}
