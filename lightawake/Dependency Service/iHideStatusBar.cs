﻿using System;
namespace lightawake
{
    public interface iHideStatusBar
    {
        void HideStatusBar(bool isHidden);
    }
}
