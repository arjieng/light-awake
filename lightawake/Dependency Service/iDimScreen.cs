﻿using System;
namespace lightawake
{
    public interface iDimScreen
    {
        void DimScreen(bool lightsOff);
    }
}
