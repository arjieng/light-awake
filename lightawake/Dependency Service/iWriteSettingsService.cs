﻿using System;
namespace lightawake
{
    public interface iWriteSettingsService
    {
        bool isWriteSettingsOn();
        void OpenSettingsUrl();
    }
}
