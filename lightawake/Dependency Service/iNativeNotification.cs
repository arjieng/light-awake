﻿using System;
namespace lightawake
{
    public interface iNativeNotification
    {
        void NativeNotif(int notifId);
        void NativeNotif(DateTime dateTime);
        void CancelNotification();
        bool RequestPermission();
    }
}
