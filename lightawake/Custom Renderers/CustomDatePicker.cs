﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace lightawake
{
    public class CustomDatePicker : BoxView
    {
        public static readonly BindableProperty TimeProperty = BindableProperty.Create("Time", typeof(TimeSpan), typeof(CustomDatePicker), new TimeSpan(0,0,0),defaultBindingMode:BindingMode.TwoWay, propertyChanged: OnPropertyChanged);

        private static void OnPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var timeValue = (CustomDatePicker)bindable;
            Debug.WriteLine("this is newValue {0}", newValue);
        }

        public TimeSpan Time
		{
			get { return (TimeSpan)GetValue(TimeProperty); }
			set { SetValue(TimeProperty, value); }
		}

	}
}
