﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Input;
using Xamarin.Forms;

namespace lightawake
{
    public class HandleTaps : INotifyPropertyChanged
    {
		int taps = 0;
		ICommand tapCommand;
		public HandleTaps()
		{
			// configure the TapCommand with a method
			tapCommand = new Command(OnTapped);
		}
		public ICommand TapCommand
		{
			get { return tapCommand; }
		}


        async void OnTapped(object s)
		{
			taps++;

             TimeSpan tt = new TimeSpan(0, 0, 0, 1);
             Device.StartTimer(tt,TestHandleFunc);
            Debug.WriteLine(taps);

		}


		bool TestHandleFunc()
		{
			if (taps > 1)
			{
				//Your action for Double Click here
                Debug.WriteLine(taps);
			}
			else
			{
				//Your action for Single Click here
				Debug.WriteLine(taps);
			}
			taps = 0;
			return false;
		}
		public event PropertyChangedEventHandler PropertyChanged;
    }
}
