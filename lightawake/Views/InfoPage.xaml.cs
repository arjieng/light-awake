﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace lightawake
{
    public partial class InfoPage : ContentPage
    {
		public static readonly BindableProperty eventIconProperty = BindableProperty.Create("addImage", typeof(string), typeof(InfoPage), "BackIcon.png");
		public static readonly BindableProperty eventColorProperty = BindableProperty.Create("addColor", typeof(Color), typeof(InfoPage), Constants.MAIN_BACKGROUNDCOLOR);
		public static readonly BindableProperty logoutButtonProperty = BindableProperty.Create("BackButton", typeof(ICommand), typeof(InfoPage), null);

		public Color addColor
		{
			get { return (Color)GetValue(eventColorProperty); }
			set { SetValue(eventColorProperty, value); }
		}

		public string addImage
		{
			get { return (string)GetValue(eventIconProperty); }
			set { SetValue(eventIconProperty, value); }
		}

		public ICommand BackButton
		{
			get { return new Command(() => { Application.Current.MainPage.Navigation.PopModalAsync(); }); }
		}



		public InfoPage()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
			DependencyService.Get<iHideStatusBar>().HideStatusBar(false);
        }

        void Website_Clicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://lightawake.biz"));

        }

        void SendEmail_Clicked(object sender, EventArgs e)
        {
            try
            {
                Device.OpenUri(new Uri("mailto:feedback@lightawake.biz?subject=Light Awake Feedback&body=Thank you for taking the time to send us your feedback!"));
            }
            catch{
                DisplayAlert("Email","No email available","Ok");
            }
        }

        void BackButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }
    }
}
