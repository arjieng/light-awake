﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;


namespace lightawake
{
    public partial class AlarmPage : ContentPage
    {
        TimeSpan snooze_time = new TimeSpan(0, 0, 10);
        Random rnd = new Random();
        AlarmMessageModel am;
       
        public AlarmPage(AlarmMessageModel alarmMessage)
        {
            InitializeComponent();
            am = alarmMessage;
            BindingContext = am;
          
            if(Device.RuntimePlatform == Device.iOS)
            {
                timePicker.IsEnabled = false;
                timePicker.IsVisible = false;
                datePicker.PropertyChanged += TimeChanged;
            }
            if(Device.RuntimePlatform == Device.Android)
            {
                datePicker.IsEnabled = false;
                datePicker.IsVisible = false;
                timePicker.PropertyChanged += TimeChanged;
            }


        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
			DependencyService.Get<iHideStatusBar>().HideStatusBar(false);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if(Device.RuntimePlatform == Device.Android)
            {
                timePicker.PropertyChanged -= TimeChanged;
            }
            if(Device.RuntimePlatform == Device.iOS)
            {
                datePicker.PropertyChanged -= TimeChanged;
            }

        }


        private void TimeChanged(object sender, PropertyChangedEventArgs e)
        {
            if(am.isAlarm && am.isCurrentlySet)
            {
                am.isCurrentlySet = false;
                am.isAlarm = false;
            }
        }

        void Toggle_Alarm()
        {
            
            if (switchButton.IsToggled)
            {
                am.isAlarm = true;
                if(Device.RuntimePlatform == Device.iOS)
                {
                    am.timeAlarm = datePicker.Time;
                }
                if(Device.RuntimePlatform == Device.Android)
                {
                    am.timeAlarm = timePicker.Time;
                }

                if(!am.isCurrentlySet)
                {
                    HomePage.triggerAlarm.Invoke(this, null);
                }

				am.isCurrentlySet = true;
            }
            else
            {
                am.isAlarm = false;
                am.isCurrentlySet = false;
                if (Device.RuntimePlatform == Device.iOS)
                {
                    DependencyService.Get<iNativeNotification>().CancelNotification();
                }
            }

        }



      

        void BackButton_Clicked(object sener, EventArgs e)
        {
			Toggle_Alarm();
            Navigation.PopModalAsync();
        }

     

       
	}
}
