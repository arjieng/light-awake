﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Threading;
using Rg.Plugins.Popup.Extensions;

namespace lightawake
{
    public partial class HomePage : ContentPage
    {
	    bool isSnooze = false;
        bool isClicked = false;
        bool isTapped = false;
        bool alarmFired = false;
        bool isDone = true;
        bool isDim = true;
        bool inSnooze = true;
        bool tapHandled;
        StackLayout snoozeStack, alarmStack,overLayStack;
		Random rnd = new Random();
        AlarmMessageModel am = new AlarmMessageModel();
        Pulsate pulse = new Pulsate();
        public static EventHandler triggerAlarm; 
        public static EventHandler resetAlarm;

        public static EventHandler overLayInfo;
        public static EventHandler overLayAlarm;
        public static EventHandler overLayTapped;

        TapGestureRecognizer overLaySingleTap;
        Button button1,button2,button3;
        CancellationTokenSource ct;

		public HomePage()
        {
            InitializeComponent();
            BindingContext = am;

            triggerAlarm += Set_Alarm;
            resetAlarm += Reset;

            overLayInfo += InfoButton_Clicked;
            overLayAlarm += AlarmButton_Clicked;
            overLayTapped += Overlay_Tapped;

            am.isCurrentlySet = false;
            TapGesture_Add();
        }

        protected override  void OnAppearing()
        {
            base.OnAppearing();
            alarmButton.IsEnabled = true;
            infoButton.IsEnabled = true;
            dimButton.IsEnabled = true;

			if (!DependencyService.Get<iNativeNotification>().RequestPermission())
			{
                DependencyService.Get<iNativeNotification>().RequestPermission();
			}
            DependencyService.Get<iHideStatusBar>().HideStatusBar(false);
            isDim = true;
            Debug.WriteLine(am.timeAlarm);
           
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

      

        async void InfoButton_Clicked(object sender, EventArgs e)
        {
			DependencyService.Get<iHideStatusBar>().HideStatusBar(false);
            DimScreen(false);
			isClicked = true;
			if (isClicked)
			{
				isClicked = false;

				if (!isDim)
				{
					var action = await DisplayActionSheet("Would you like to leave the alarm screen?", null, null, "Yes", "No");
					if (action == "Yes")
					{
                        infoButton.IsEnabled = false;
						Reset(this, null);
						await Navigation.PushModalAsync(new InfoPage());
					}
					else
					{
						//DependencyService.Get<iDimScreen>().DimScreen(true);
                        DimScreen(true);
					}

				}
				else
				{
					infoButton.IsEnabled = false;
					Reset(this, null);
					await Navigation.PushModalAsync(new InfoPage());
				}
			}
        }
		

        async void AlarmButton_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<iHideStatusBar>().HideStatusBar(false);
    		isClicked = true;
    		if (isClicked)
    		{
                isClicked = false;
                Debug.WriteLine(isDim);
    			if (!isDim)
    			{
    				var action = await DisplayActionSheet("Would you like to leave the alarm screen?", null, null, "Yes", "No");
                    if(action == "Yes")
                    {
    					alarmButton.IsEnabled = false;
    					Reset(this, null);
                        await Navigation.PushModalAsync(new AlarmPage(am));
                    }
                    else
                    {
                        //DependencyService.Get<iDimScreen>().DimScreen(true);
                        DimScreen(true);
                    }
    			}
                else
    			{
    				alarmButton.IsEnabled = false;
    				Reset(this, null);
                    await Navigation.PushModalAsync(new AlarmPage(am));
                }
    		}

		}

		void DimButton_Clicked(object sender, EventArgs e)
		{
            isClicked = true;
            if(isClicked)
            {
                isClicked = false;
				if (isDim)
				{
					DimScreen(isDim);
					isDim = false;
				}
				else
				{
					DimScreen(isDim);
					isDim = true;
				}
            }
			
		}

        async void DimScreen(bool dim)
        {
            if(dim)
            {
                overLayStack = new StackLayout()
                {
                    BackgroundColor = Color.FromRgba(0, 0, 0, .950),
                    Opacity = 0
                };

                var buttonStack = new StackLayout()
                {
                    BackgroundColor = Color.Transparent,
                    Orientation = StackOrientation.Horizontal,
                    Spacing = 0,
                    VerticalOptions = LayoutOptions.EndAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    Padding = 0
                };

                overLayStack.Children.Add(buttonStack);

                button1 = new Button()
                {
                    BackgroundColor = Color.Transparent,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    HeightRequest = 90.ScaleHeight()

                };
				button2 = new Button()
				{
					BackgroundColor = Color.Transparent,
					HorizontalOptions = LayoutOptions.FillAndExpand,
                    HeightRequest = 90.ScaleHeight()
				};
				 button3 = new Button()
				{
					BackgroundColor = Color.Transparent,
					HorizontalOptions = LayoutOptions.FillAndExpand,
                    HeightRequest = 90.ScaleHeight()
				};

                button1.Clicked += OverlayInfo_Clicked;
                button2.Clicked += OverlayAlarm_Clicked;
                button3.Clicked += OverlayDim_Clicked;

                buttonStack.Children.Add(button1);
				buttonStack.Children.Add(button2);
				buttonStack.Children.Add(button3);

			    overLaySingleTap = new TapGestureRecognizer();
				overLaySingleTap.NumberOfTapsRequired = 1;
				overLaySingleTap.Tapped += Overlay_Tapped;
                overLayStack.GestureRecognizers.Add(overLaySingleTap);

                isTapped = false;
                overLayStack.Children.Add(buttonStack);
				mainGrid.Children.Insert(2, overLayStack);
                await overLayStack.FadeTo(1, 700, Easing.CubicIn);
            }
          
            DependencyService.Get<iHideStatusBar>().HideStatusBar(true);

        }

        void OverlayInfo_Clicked(object sender, EventArgs e)
        {
            if (!isTapped)
			{
                isDim = false;
				isTapped = true;
				mainGrid.Children.Remove(overLayStack);
                infoButton.IsEnabled = false;
                overLayInfo.Invoke(this, null);
			}
        }


		void OverlayAlarm_Clicked(object sender, EventArgs e)
		{
            if(!isTapped)
            {
                isDim = false;
                isTapped = true;
                mainGrid.Children.Remove(overLayStack);
                alarmButton.IsEnabled = false;
                overLayAlarm.Invoke(this, null);
            }
		}

        async void OverlayDim_Clicked(object sender, EventArgs e)
        {
            isClicked = true;
            //button3.Clicked -= OverlayDim_Clicked;
            if(isClicked)
            {
                isClicked = false;
				await overLayStack.FadeTo(0, 700, Easing.CubicOut);
				mainGrid.Children.Remove(overLayStack);
				isDim = true;
            }
        }

		

        async void Overlay_Tapped(object sender, EventArgs e)
        {
            isClicked = true;
            overLaySingleTap.Tapped -= Overlay_Tapped;
            if(isClicked)
            {
                isClicked = false;
				await overLayStack.FadeTo(0, 700, Easing.CubicOut);
				mainGrid.Children.Remove(overLayStack);
				DependencyService.Get<iHideStatusBar>().HideStatusBar(false);
				isDim = true;
                alarmButton.IsEnabled = true;
                infoButton.IsEnabled = true;
            }

        }

		void AlarmMessage()
		{
			alarmStack = new StackLayout()
			{
				Spacing = 0,
				IsVisible = true,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
				BackgroundColor = Color.FromHex("#60fdff"),
                Margin = new Thickness(0, 0, 0, 35.scale())
			};

			var label1 = new Label()
			{
				Text = "Tap Once to Snooze",
				HorizontalOptions = LayoutOptions.Center,
				TextColor = Color.White,
                FontSize = 20.scale(),
				FontAttributes = FontAttributes.Bold
			};

			var label2 = new Label()
			{
				Text = "Double Tap to Turn Off",
				HorizontalOptions = LayoutOptions.Center,
				TextColor = Color.White,
                FontSize = 20.scale(),
				FontAttributes = FontAttributes.Bold
			};

			alarmStack.Children.Add(label1);
			alarmStack.Children.Add(label2);
			firstStack.Children.Insert(3, alarmStack);
		}

		void SnoozeMessage()
		{
			snoozeStack = new StackLayout()
			{
				IsVisible = true,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};

			var label1 = new Label()
			{
				Text = "-SNOOZING-",
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start,
				TextColor = Color.White,
				BackgroundColor = Color.FromHex("#60fdff"),
				FontSize = 20.scale(),
				FontAttributes = FontAttributes.Bold
			};

			var label2 = new Label()
			{
				Text = "Double Tap to Cancel Snooze",
				HorizontalOptions = LayoutOptions.Center,
				TextColor = Color.White,
				BackgroundColor = Color.FromHex("#60fdff"),
                FontSize = 20.scale(),
				FontAttributes = FontAttributes.Bold,
                Margin = new Thickness(0, 85.scale(), 0, 0)
			};

			snoozeStack.Children.Add(label1);
			snoozeStack.Children.Add(label2);
			firstStack.Children.Insert(3, snoozeStack);

		}

        async void Reset(object sender, EventArgs e)
        {
            if (alarmFired || isSnooze )
			{
				await pulse.PulseOn(firstStack, false);
                if(alarmStack != null )
                {
                    Remove_InsertedStack(alarmStack);
                }
                if (snoozeStack != null)
				{
					Remove_InsertedStack(snoozeStack);
				}
				isSnooze = false;
				am.isAlarm = false;
                inSnooze = true;
                alarmFired = false;
                am.isCurrentlySet = false;
				alarmButton.IsEnabled = true;
				DependencyService.Get<iAudio>().PlayAudioFile(false);
			}

        }

        async void Alarm_Fired(CancellationToken token)
        {
            if(Device.RuntimePlatform == Device.Android)
            {
               DependencyService.Get<iNativeNotification>().NativeNotif(rnd.Next()); 
            }
            alarmFired = true;
            if(overLayStack != null)
            {
               HomePage.overLayTapped.Invoke(this, null); 
            }

			AlarmMessage();
            Device.BeginInvokeOnMainThread(async() => {
				try
				{
					await Task.Delay(30000, token);
					if (am.isAlarm)
					{
						DependencyService.Get<iAudio>().PlayAudioFile(true);
					}

				}
				catch (OperationCanceledException)
				{
					Debug.WriteLine("Alarm is turned off without triggering alarm sound.");
				}
				catch (Exception)
				{
					Debug.WriteLine("Cancelled");
				}
				ct = null;
				//await Task.Delay(30000, token);
				//if (am.isAlarm)
				//{
				//	DependencyService.Get<iAudio>().PlayAudioFile(true);
				//}

            });
			
			await pulse.PulseOn(firstStack, true);
			DependencyService.Get<iAudio>().PlayAudioFile(false);
			Remove_InsertedStack(alarmStack);
			am.isAlarm = false;
        }


		async void Set_Alarm(object sender, EventArgs e)
        {
            var alarmDay = DateTime.Now.Day;

            alarmFired = false;
            if (am.timeAlarm.Hours <= DateTime.Now.Hour)
            {
            	if (am.timeAlarm.Minutes <= DateTime.Now.Minute)
            	{
            		alarmDay = DateTime.Now.Day + 1;
            	}
                if(am.timeAlarm.Minutes > DateTime.Now.Minute  && am.timeAlarm.Hours != DateTime.Now.Hour )
                {
                    alarmDay = DateTime.Now.Day + 1;
                }
            }
			var limitTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, alarmDay, int.Parse(am.timeAlarm.Hours.ToString()), int.Parse(am.timeAlarm.Minutes.ToString()), 0);
            Debug.WriteLine("this is limitTime:{0}",limitTime);
			if (Device.RuntimePlatform == Device.iOS)
			{
				DependencyService.Get<iNativeNotification>().NativeNotif(limitTime);
			}
			while (DateTime.Now.CompareTo(limitTime) != 1 && am.isAlarm)
			{
				await Task.Delay(1000);
			}

			if (am.isAlarm)
			{
				ct = new CancellationTokenSource();
                Alarm_Fired(ct.Token);
			}
           
        }

        void TapGesture_Add()
        {
            var singleTap = new TapGestureRecognizer();
            singleTap.NumberOfTapsRequired = 1;
            singleTap.Tapped += SingleTap_Tapped;
            mainView.GestureRecognizers.Add(singleTap);

			var doubleTap = new TapGestureRecognizer();
			doubleTap.NumberOfTapsRequired = 2;
			doubleTap.Tapped += DoubleTap_Tapped;
			mainView.GestureRecognizers.Add(doubleTap);

        }

         void DoubleTap_Tapped(object sender, EventArgs e)
        {
			tapHandled = true;
            Debug.WriteLine("2 Taps");
            if(am.isAlarm || isSnooze)
            {
                Reset(this,null);
                if(ct != null)
                {
                    ct.Cancel();
                }

            }
		}

        async void SingleTap_Tapped(object sender, EventArgs e)
        {
            tapHandled = false;
            Device.StartTimer(new TimeSpan(0,0,0,0,300),TapTimer);

            if (!isDim)
            {
                isDim = true;
            }

            if (alarmFired)
                isSnooze = true;

            if (isSnooze)
            {
                alarmFired = false;
                if(inSnooze)
                {
                    inSnooze = false;
					am.isAlarm = false;
					await pulse.PulseOn(firstStack, false);
					Remove_InsertedStack(alarmStack);
					SnoozeMessage();
					while (isSnooze)
					{
						Debug.WriteLine("Start");
						DependencyService.Get<iAudio>().PlayAudioFile(false);
						await Task.Delay(480000);
                        if (isSnooze)
                        {
							if (overLayStack != null)
							{
								HomePage.overLayTapped.Invoke(this, null);
							}
		                    await Task.Run(() =>
		                    {
		                        Device.BeginInvokeOnMainThread(async () =>
		                        {
		                            await pulse.PulseOn(firstStack, isSnooze);
		                        });
		                    });
		                    await Task.Delay(30000);
		                    if(isSnooze)
		                    {
							    DependencyService.Get<iAudio>().PlayAudioFile(true);
							}
							//if (snoozeStack != null)
							//{
							//	Remove_InsertedStack(snoozeStack);
							//}
       //                     am.isAlarm = true;
       //                     inSnooze = true;
							//if (am.isAlarm)
							//{
							//	ct = new CancellationTokenSource();
							//	try
							//	{
							//		Alarm_Fired(ct.Token);
							//	}
							//	catch (OperationCanceledException)
							//	{
							//		Debug.WriteLine("Alarm is turned off without triggering alarm sound.");
							//	}
							//	catch (Exception)
							//	{
							//		Debug.WriteLine("Cancelled");
							//	}
							//	ct = null;
							//}
                        }
						
						Debug.WriteLine("Wait");
						await Task.Delay(300000);
					}
                   
                }
				
            }

        }

        bool TapTimer()
        {
			if (!tapHandled)
			{
				tapHandled = true;
                Debug.WriteLine("a single tap");
			}
			return false;
        }

       void Remove_InsertedStack(StackLayout insertedStack)
        {
			if (insertedStack != null)
			{
                firstStack.Children.Remove(insertedStack);
			}
        }
    }
}
