﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace lightawake.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
			App.screenWidth = (float)UIScreen.MainScreen.Bounds.Width;
			App.screenHeight = (float)UIScreen.MainScreen.Bounds.Height;
			App.appScale = (float)UIScreen.MainScreen.Scale;
			App.DeviceType = 0;

            global::Xamarin.Forms.Forms.Init();

            // Code for starting up the Xamarin Test Cloud Agent
#if DEBUG
			//Xamarin.Calabash.Start();
#endif

            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }

		
    }
}
