﻿
using System;
using System.Linq;
using AVFoundation;
using CoreGraphics;
using Foundation;
using UIKit;
namespace lightawake.iOS
{
    public class UIPickerView : UIView
    {
        UIDatePicker datePicker;
        DateTime dateTime;
        public UIPickerView(DateTime _date)
        {
            dateTime = _date;
            Create_DatePicker();
            this.BackgroundColor = UIColor.Red;
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            datePicker.Frame = datePicker.Bounds = rect;
        }

        void Create_DatePicker()
        {
			datePicker = new UIDatePicker();
			datePicker.Mode = UIDatePickerMode.Time;
			datePicker.Locale = new NSLocale("no_nb");
			datePicker.Date = (NSDate)dateTime;
            this.AddSubview(datePicker);
        }

    }
}
