﻿using System;
using lightawake;
using lightawake.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using UIKit;
[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace lightawake.iOS
{
    public class CustomButtonRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

        }
    }
}
