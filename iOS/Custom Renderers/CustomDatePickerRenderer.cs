﻿using System;
using lightawake;
using lightawake.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using UIKit;
using CoreGraphics;
using Foundation;
using System.Diagnostics;

[assembly: ExportRenderer(typeof(CustomDatePicker), typeof(CustomDatePickerRenderer))]
namespace lightawake.iOS
{
    public class CustomDatePickerRenderer : ViewRenderer
    {
        CustomDatePicker customDatePicker;
        UIDatePicker datePicker = new UIDatePicker();
        NSLocale locale;
 
        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
			base.OnElementChanged(e);
            customDatePicker = (CustomDatePicker)Element;
           
            if(Control == null)
            {
                datePicker.Mode = UIDatePickerMode.Time;
                datePicker.Frame = new CGRect(Element.Bounds.X, Element.Bounds.Y,UIScreen.MainScreen.Bounds.Width, Element.Height);
                datePicker.MaximumDate = ConvertDateTimeToNSDate(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59));
                datePicker.MinimumDate = ConvertDateTimeToNSDate(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0));
                datePicker.BackgroundColor = Xamarin.Forms.Color.FromHex("#ecdbb2").ToUIColor();
				SetNativeControl(datePicker);
            }

            if(e.OldElement != null)
            {
                datePicker.ValueChanged -= DatePicker_ValueChanged;
            }

            if(e.NewElement != null)
            {
                datePicker.ValueChanged += DatePicker_ValueChanged;
                customDatePicker.Time = datePicker.Date.ToDateTime().ToLocalTime().TimeOfDay;
            }
			
        }

        void DatePicker_ValueChanged(object sender, EventArgs e)
        {
            customDatePicker.Time = datePicker.Date.ToDateTime().ToLocalTime().TimeOfDay;
        }
     
        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == VisualElement.HeightProperty.PropertyName || e.PropertyName == VisualElement.WidthProperty.PropertyName|| e.PropertyName == CustomDatePicker.TimeProperty.PropertyName)
			{
               
				this.SetNeedsDisplay();
			}
		}

		public NSDate ConvertDateTimeToNSDate(DateTime date)
		{
			DateTime newDate = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(2001, 1, 1, 0, 0, 0));

			return NSDate.FromTimeIntervalSinceReferenceDate((date - newDate).TotalSeconds);
		}

    }
}
