﻿using System;
using lightawake;
using lightawake.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using UIKit;
[assembly: ExportRenderer(typeof(CustomTimePicker), typeof(CustomTimePickerRenderer))]
namespace lightawake.iOS
{
    public class CustomTimePickerRenderer : TimePickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<TimePicker> e)
        {
            base.OnElementChanged(e);
           
        }

        public override void Draw(CoreGraphics.CGRect rect)
        {
            base.Draw(rect);
        }
    }
}
