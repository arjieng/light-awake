﻿using System;
using Foundation;
using lightawake.iOS;
using UIKit;
using UserNotifications;
using Xamarin.Forms;

[assembly: Dependency(typeof(IOSNotification))]
namespace lightawake.iOS
{
    public class IOSNotification : iNativeNotification
    {
        bool success;
        public IOSNotification()
        {
        }

		public bool RequestPermission()
		{
			var center = UNUserNotificationCenter.Current;
			var options = UNAuthorizationOptions.Alert | UNAuthorizationOptions.Sound | UNAuthorizationOptions.Badge;


			center.RequestAuthorization(options, (granted, error) => {
                if (granted)
                {
                    success = true;
                }
                else
                {
                    success = false;
                }

			});
            return success;
		}

        public void NativeNotif(int notifId)
        {
            

        }

        public void NativeNotif(DateTime dateTime)
        {
            var content = new UNMutableNotificationContent();
            var dateInfo = new NSDateComponents();

            content.Title = "Light Awake Alarm";
            content.Body = "Next time leave Light Awake open! Use the dim button to sleep.";

            dateInfo.Month = dateTime.Month;
            dateInfo.Day = dateTime.Day;
            dateInfo.Hour = dateTime.Hour;
            dateInfo.Minute = dateTime.Minute;

            var trigger = UNCalendarNotificationTrigger.CreateTrigger(dateInfo, false);
            var requestID = "sampleRequest";
            var request = UNNotificationRequest.FromIdentifier(requestID, content, trigger);

            UNUserNotificationCenter.Current.AddNotificationRequest(request, (err) => {
                if (err != null)
                {
                    // Do something with error...
                }
            });
        }

        public void CancelNotification()
        {
            UNUserNotificationCenter.Current.RemoveAllPendingNotificationRequests();
        }
    }
}
