﻿using System;
using CoreGraphics;
using lightawake.iOS;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(HideStatusBarService))]
namespace lightawake.iOS
{
    public class HideStatusBarService : iHideStatusBar
    {
        static bool hidden;
        public HideStatusBarService()
        {
        }

        public void HideStatusBar(bool isHidden)
        {
            hidden = isHidden;
            if(hidden)
            {
                UIApplication.SharedApplication.StatusBarHidden = true;
            }
            else
            {
                UIApplication.SharedApplication.StatusBarHidden = false;
            }

        }
    }
}
