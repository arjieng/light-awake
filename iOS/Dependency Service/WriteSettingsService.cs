﻿using System;
using lightawake.iOS;
using UIKit;
using UserNotifications;
using Xamarin.Forms;

[assembly: Dependency(typeof(WriteSettingsService))]
namespace lightawake.iOS
{
    public class WriteSettingsService : iWriteSettingsService
    {
        public WriteSettingsService()
        {
        }

        public bool isWriteSettingsOn()
        {
            return true;
        }

        public void OpenSettingsUrl()
        {
            
        }
    }
}
