﻿using System;
using CoreGraphics;
using lightawake.iOS;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(DimBrightness))]
namespace lightawake.iOS
{
    public class DimBrightness : iDimScreen
    {
        bool lights;
        CGRect frame;
        UIView filter;
        public DimBrightness()
        {
			frame = new CGRect(0, 0, App.screenWidth, App.screenHeight);
		    filter = new UIView();
			filter.Frame = frame;
			filter.BackgroundColor = UIColor.Black;
			filter.Alpha = 0.965f;
			filter.UserInteractionEnabled = false;
        }

        public void DimScreen(bool lightsOff)
        {
            lights = lightsOff;
            if(lights)
            {
                UIApplication.SharedApplication.KeyWindow.RootViewController.View.AddSubview(filter);
                UIApplication.SharedApplication.StatusBarHidden = true;
            }
            else
			{
                filter.RemoveFromSuperview();
                UIApplication.SharedApplication.StatusBarHidden = false;
			}
        }

    }



}
