﻿using System;
using CoreGraphics;
using lightawake.iOS;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(DisableSleepService))]
namespace lightawake.iOS
{
    public class DisableSleepService : iDisableSleep
    {
        public void DisableSleep()
        {
            UIApplication.SharedApplication.IdleTimerDisabled = true;
        }
    }
}
